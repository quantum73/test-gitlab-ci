from src.main import sum_two

def test_sum_two():
    res = sum_two(2, 2)
    assert 4 == res
